## Purpose
The purpose of this project initially was to apply techniques learned from Andrew Ng's Machine Learning course to something I'm interested in. I decided I'd be curious to see if these techniques could be applied to trading data from a game called Old School RuneScape.

The goal is to feed some trading data as input and to receive output of predicted future trading data.

HOWEVER, as I started the project, I realized I can use it to understand a lot more than just Machine Learning techniques. I've got a list of things to implement for the sake of learning in this project, including:

*	SQL and NoSQL dbs (MSSQL, MongoDB)
*	Micro and regular ORMs (Dapper, NHibernate, mayyyybe LINQ to SQL or Entity Framework)
*	XUnit tests